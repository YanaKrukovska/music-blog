import './App.css';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import CustomNavbar from "./components/CustomNavbar"
import Gallery from "./pages/Gallery"
import Contacts from "./pages/Contacts";
import Main from "./pages/Main";
import AboutUs from "./pages/AboutUs";
import ErrorPage from "./pages/ErrorPage";

function App() {
    return (
        <Router>
            <CustomNavbar/>
            <Routes>
                <Route path="/" element={<Main/>}/>
                <Route path="/gallery" element={<Gallery/>}/>
                <Route path='/contacts' element={<Contacts/>}/>
                <Route path='/about-us' element={<AboutUs/>}/>
                <Route path="*" element={<ErrorPage statusCode={404} errorMessage={"Not Found"}/>}/>
            </Routes>
        </Router>
    );
}

export default App;
