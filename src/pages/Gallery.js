import React from 'react';
import "../css/Gallery.css";
import data from '../data.json';

const Gallery = () => {
    const photos = data.map((post) => {
        return (
            <img key={post.image}
                 className="gallery-photo"
                 src={post.image}
                 alt={post.title}
            />
        )
    })
    return (
        <div className="gallery">
            {photos}
        </div>
    );
}
export default Gallery;
