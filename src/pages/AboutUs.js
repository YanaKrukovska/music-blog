import React from "react";
import '../css/AboutUs.css';
import data from '../about-us.json';

const AboutUs = () => {

    let paragraphs = [];
    for (let i = 0; i < data.length; i++) {
        paragraphs.push(<p key={i}>{data[i].paragraph}</p>);
    }

    return (
        <div>
            <h2>About Us</h2>
            <div className="paragraphs">
                {paragraphs}
            </div>
        </div>
    );
}
export default AboutUs;
