import React from 'react';
import {MapContainer, Marker, Popup, TileLayer} from 'react-leaflet';
import '../css/Contacts.css'
import 'leaflet/dist/leaflet.css';
import marker from '../assets/Marker.svg';
import {Icon} from 'leaflet'

const position = [50.464382162145, 30.519224658472915]

const myIcon = new Icon({
    iconUrl: marker,
    iconSize: [32, 32]
})

const Contacts = () => {
    return (
        <div>
            <h2>How to find us</h2>
            <div className="map-placeholder">
                <MapContainer center={position} zoom={16} scrollWheelZoom={false}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={position} icon={myIcon}>
                        <Popup>NauKMA</Popup>
                    </Marker>
                </MapContainer>
            </div>
        </div>
    );
}
export default Contacts;
