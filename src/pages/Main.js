import Post from "../components/Post";
import data from '../data.json';
import React, {useState} from "react";
import Pagination from 'react-bootstrap/Pagination';
import "../css/Main.css"
import {Button, Carousel, Form} from "react-bootstrap";

function ShowSlides({interval}) {
    const posts = data.slice(0, 3);
    const slides = posts.map((post) => {
        return (
            <Carousel.Item interval={interval} key={post.title}>
                <img
                    className="slide d-block w-100"
                    src={post.image}
                    alt={post.title}
                />
                <Carousel.Caption>
                    <h3>{post.title}</h3>
                </Carousel.Caption>
            </Carousel.Item>
        )
    })
    return <Carousel>{slides}</Carousel>;
}

function CustomPagination({posts, RenderComponent, postsPerPage}) {
    const [currentPage, setCurrentPage] = useState(1);
    const pageAmount = Math.round(posts.length / postsPerPage);

    function goToNextPage() {
        setCurrentPage((page) => page + 1);
    }

    function goToPreviousPage() {
        setCurrentPage((page) => page - 1);
    }

    function changePage(event) {
        setCurrentPage(Number(event.target.textContent));
    }

    const getPaginatedData = () => {
        const startIndex = currentPage * postsPerPage - postsPerPage;
        const endIndex = startIndex + postsPerPage;
        return posts.slice(startIndex, endIndex);
    };

    const getPaginationGroup = () => {
        let start = Math.floor((currentPage - 1) / pageAmount) * pageAmount;
        return new Array(pageAmount).fill().map((_, idx) => start + idx + 1);
    };

    return (
        <div>
            <div>
                {getPaginatedData().map((d, idx) => (
                    <RenderComponent key={idx} body={d}/>
                ))}

                <Pagination>
                    {pageAmount > 0 &&
                    <Pagination.Prev onClick={goToPreviousPage}
                                     active={currentPage !== 1}
                                     disabled={currentPage === 1}/>
                    }

                    {getPaginationGroup().map((item, index) => (
                        <Pagination.Item key={index} onClick={changePage} active={index === (currentPage - 1)}>{item}
                        </Pagination.Item>
                    ))}
                    {pageAmount > 0 &&
                    <Pagination.Next onClick={goToNextPage} active={currentPage !== pageAmount}
                                     disabled={currentPage === pageAmount}/>
                    }
                </Pagination>
            </div>
        </div>
    );
}

const Main = () => {
    const [sortedData, setSortedData] = useState(data);

    function handleSubmit(e) {
        e.preventDefault();

        let newSorted = [];
        let searchInput = e.target.searchInput.value;

        if (searchInput !== '') {
            for (const post of data) {
                for (const artist of post.artists) {
                    if (artist.toLowerCase() === searchInput.toLowerCase()) {
                        newSorted.push(post);
                    }
                }
            }
            setSortedData(newSorted);
        } else {
            setSortedData(data);
        }
    }

    return (
        <main>
            <ShowSlides interval={2000}/>
            <div>
                <Form className="search-form" onSubmit={(e) => handleSubmit(e)}>
                    <Form.Group className="mb-3" controlId="searchInput">
                        <Form.Control type="text" name="searchInput" placeholder="Enter artist name"/>
                    </Form.Group>
                    <Button variant="dark" type="submit">Search</Button>
                </Form>
            </div>
            <CustomPagination
                posts={sortedData}
                RenderComponent={Post}
                postsPerPage={3}
            />
        </main>
    );
};

export default Main;
