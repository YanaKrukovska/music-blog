import '../css/Post.css';
import Card from 'react-bootstrap/Card';
import {Image} from "react-bootstrap";

const Post = ({body}) => {
    return (
        <div>
            <Card className="post-card">
                <Image className="photo"
                       src={body.image} alt="image"
                />
                <h3>{body.title}</h3>
                <p>{body.text}</p>
                <div className="links">
                    <a className="playlist-link" href={body.spotify} target="_blank" rel="noreferrer">Spotify</a>
                    <a className="playlist-link" href={body.youtubeMusic} target="_blank" rel="noreferrer">Youtube Music</a>
                </div>
            </Card>
        </div>
    )
};

export default Post;
